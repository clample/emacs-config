;; Bootstrap straight.el
;; See https://github.com/raxod502/straight.el/tree/af5437f2afd00936c883124d6d3098721c2d306c#getting-started
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq straight-use-package-by-default t)
(straight-use-package 'use-package)

;; Needed to find executables on macos
(use-package exec-path-from-shell
  :config
  (when (memq window-system '(mac ns x))
    (exec-path-from-shell-initialize))
  :ensure t)


(use-package direnv
    :ensure t
    :config
    (setq direnv-always-show-summary nil)
    (direnv-mode)
    ;; Sometimes direnv is loaded too late.
    ;; See https://github.com/wbolster/emacs-direnv/issues/17
    ;; To resolve, set 'prog-mode-hook.
    (add-hook 'prog-mode-hook #'direnv-update-environment))

(use-package magit
  :ensure t
  :config
  (setq transient-default-level 5)
    :bind
    (("C-x g" . magit-status)
    ("C-c g" . magit-file-dispatch)))

(when window-system
    (tool-bar-mode -1)
    (scroll-bar-mode -1)
    (menu-bar-mode -1))

(setq inhibit-startup-screen t)

(use-package pdf-tools
    :ensure t
    :init
    (pdf-loader-install))

(use-package pdf-view-restore
  :after pdf-tools
  :config
  (add-hook 'pdf-view-mode-hook 'pdf-view-restore-mode)
  (setq pdf-view-restore-filename "~/.emacs.d/.pdf-view-restore"))

(use-package tex-mode
  :straight nil
  :ensure auctex
  :init
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  
  ;; Integrate with pdf-tools
  ;; See https://emacs.stackexchange.com/questions/21755/use-pdfview-as-default-auctex-pdf-viewer/21764#21764
  (setq TeX-view-program-selection '((output-pdf "PDF Tools"))
        TeX-view-program-list '(("PDF Tools" TeX-pdf-tools-sync-view))
        TeX-source-correlate-start-server t)
  (add-hook 'TeX-after-compilation-finished-functions
            #'TeX-revert-document-buffer))

(setq-default indent-tabs-mode nil)

(use-package org-roam
    :ensure t
    :init
    (setq org-roam-v2-ack t)
    :custom
    (org-roam-directory "~/notes")
    :config
    (org-roam-db-autosync-mode)
    (require 'org-roam-protocol)
    :bind (("C-c o f" . org-roam-node-find)
           (:map org-mode-map
                 (("C-c o i" . org-roam-node-insert)
                  ("C-c o o" . org-id-get-create)
                  ("C-c o t" . org-roam-tag-add)
                  ("C-c o a" . org-roam-alias-add)
                  ("C-c o l" . org-roam-buffer-toggle)))))
(server-start)

(use-package org-ref
    :ensure t
    :config
    (setq reftex-default-bibliography '("~/notes/bibliography.bib"))
    (setq org-ref-default-bibliography '("~/notes/bibliography.bib")
        org-ref-pdf-directory "~/papers/"))

(setq bibtex-completion-bibliography
  '("~/notes/bibliography.bib"))
(setq bibtex-completion-library-path '("~/papers"))

(use-package org-drill
    :ensure t
    :config
    (setq org-drill-maximum-items-per-session nil)
    (setq org-drill-maximum-duration nil)
    (setq org-drill-learn-fraction 0.5))

(setq org-format-latex-options (plist-put org-format-latex-options :scale 2.0))
(setq org-startup-with-latex-preview 'true)

(with-eval-after-load 'org
    (add-hook 'org-mode-hook #'visual-line-mode))

;; https://emacs.stackexchange.com/a/37927
(defun create-image-with-background-color (args)
  "Specify background color of Org-mode inline image through modify `ARGS'."
  (let* ((file (car args))
         (type (cadr args))
         (data-p (caddr args))
         (props (cdddr args)))
    ;; get this return result style from `create-image'
    (append (list file type data-p)
            (list :background "white")
            props)))

(advice-add 'create-image :filter-args
            #'create-image-with-background-color)

(setq org-startup-with-inline-images 'true)

(setq org-agenda-files '("~/notes"))

(setq org-agenda-custom-commands '(
    ("n" "Agenda tasks" ((agenda) (tags "-drill")))
))

(load-theme 'wheatgrass t)

(use-package nov
    :init
    (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
    :config
    (setq nov-text-width 80))

(global-set-key (kbd "C-'") #'imenu-list-smart-toggle)
(setq imenu-list-focus-after-activation t)

(use-package lsp-mode
  :ensure t
  :init
  (setq lsp-keymap-prefix "C-x l")
  :config
  (setq gc-cons-threshold 100000000)
  (setq read-process-output-max (* 1024 1024)))

(use-package lsp-treemacs
    :ensure t
    :after (lsp-mode treemacs))

(use-package yasnippet
    :ensure t
    :init
    (setq yas-snippet-dirs '("~/.emacs.d/mysnippets"))
    (yas-global-mode 1)
    :bind (:map yas-minor-mode-map
        ("\C-cy\C-s" . 'yas-insert-snippet)
        ("\C-cy\C-n" . 'yas-new-snippet)
        ("\C-cy\C-v" . 'yas-visit-snippet-file)))

(use-package bongo
    :ensure t
    :config
    (setq bongo-default-directory "~/Music")
    (setq bongo-insert-whole-directory-trees t)
    (setq bongo-logo nil)
    (setq bongo-display-track-icons nil)
    (setq bongo-display-track-lengths nil)
    (setq bongo-display-header-icons nil)
    (setq bongo-display-playback-mode-indicator nil)
    (setq bongo-display-inline-playback-progress nil)
    (setq bongo-join-inserted-tracks nil)
    (setq bongo-field-separator (propertize " · " 'face 'shadow))
    (setq bongo-mark-played-tracks t)
    (setq bongo-header-line-mode nil)
    (setq bongo-mode-line-indicator-mode nil)
    (setq bongo-enabled-backends '(vlc))
    (setq bongo-vlc-program-name "cvlc")
    (add-hook 'dired-mode-hook 
    (lambda () (when (string-match-p "\\`~/Music/" default-directory) (bongo-dired-library-mode 1)))))

(use-package rust-mode
    :ensure t
    :hook (rust-mode . lsp))

(use-package scala-mode
  :ensure t
  :interpreter
  ("scala" . scala-mode)
  :hook (scala-mode . lsp))

(use-package treemacs
    :ensure t
    :bind 
    ((("C-x t t" . treemacs))))

(use-package treemacs-projectile
  :after (treemacs projectile)
  :ensure t)

(use-package treemacs-all-the-icons
  :after (treemacs)
  :ensure t
  :config
  (treemacs-load-theme "all-the-icons"))

;; Needed by projectile for ripgrep searching
(use-package rg
  :ensure t)

(use-package projectile
    :ensure t
    :init
    (projectile-mode +1)
    :bind (:map projectile-mode-map
                ("C-x p" . projectile-command-map))
    :config
    (add-to-list 'projectile-globally-ignored-directories "*node_modules"))

(use-package all-the-icons-dired
    :ensure t
    :hook (dired-mode . all-the-icons-dired-mode))

;; Emacs creates backups and autosave files.
;; To keep things clean, we can put those in the emacs directory.
;; See https://www.youtube.com/watch?v=XZjyJG-sFZI
(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))
;; auto-save-mode doesn't create the path automatically
(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)
(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory) 
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

(use-package yaml-mode
  :ensure t
  :init
  (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode)))
(put 'downcase-region 'disabled nil)


(use-package js
  :init
  (setq js-indent-level 2)
  :config
  ;; See https://github.com/emacs-lsp/lsp-mode/issues/2770
  (define-key js-mode-map (kbd "M-.") nil))


(use-package dumb-jump
  :ensure t
  :init
  (setq dumb-jump-force-searcher 'rg)  
  (setq xref-backend-functions '(dumb-jump-xref-activate)))

