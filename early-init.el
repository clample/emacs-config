;; Avoid conflicts between straight.el and package.el
;; https://github.com/raxod502/straight.el/tree/af5437f2afd00936c883124d6d3098721c2d306c#getting-started
(setq package-enable-at-startup nil)
